import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

  private heroes: Hero[] = [
    {
      nome: "Aquaman",
      bio: "O poder mais reconhecido do Aquaman é a capacidade telepática de se comunicar com a vida marinha, que pode convocar a grandes distâncias.",
      img: "assets/img/aquaman.png",
      aparicao: "1941-11-01",
      casa: "DC"
    },
    {
      nome: "Batman",
      bio: "As principais características do Batman resumem-se em 'proezas físicas, habilidades dedutivas e obsessão'. A maioria das características básicas dos quadrinhos variam de acordo com as diferentes interpretações que foram dadas ao personagem.",
      img: "assets/img/batman.png",
      aparicao: "1939-05-01",
      casa: "DC"
    },
    {
      nome: "Demolidor",
      bio: "Tendo perdido a visão, os quatro sentidos restantes do Demolidor foram intensificados pela radiação a níveis sobre-humanos, no acidente que teve quando criança. Apesar de sua cegueira, ele pode ver através de um sexto sentido que o serve como um radar semelhante ao dos morcegos. ",
      img: "assets/img/daredevil.png",
      aparicao: "1964-01-01",
      casa: "Marvel"
    },
    {
      nome: "Hulk",
      bio: "Seu principal poder é a habilidade de aumentar sua força a níveis virtualmente ilimitados enquanto aumenta sua fúria. Dependendo de qual personalidade Hulk está no comando no momento (o Hulk Banner é o mais fraco, mas ele compensa com sua inteligência ). ",
      img: "assets/img/hulk.png",
      aparicao: "1962-05-01",
      casa: "Marvel"
    },
    {
      nome: "Lanterna Verde",
      bio: "Possuidor do anel de poder que possui a habilidade de criar manifestações de luz sólida através da utilização do pensamento. É alimentado pela Chama Verde (revisada por escritores posteriores como um poder místico chamado Coração Estelar), uma chama mágica contida dentro uma orbe (a orbe era na verdade um meteorito de metal verde que caiu na Terra, que um fabricante de lâmpadas chamado Chang encontrou) ",
      img: "assets/img/green-lantern.png",
      aparicao: "1940-06-01",
      casa: "DC"
    },
    {
      nome: "Homem-Aranha",
      bio: "Depois de ser mordido por uma aranha radioativa, ele obteve os seguintes poderes sobre-humanos, grande força, agilidade, sendo capaz de escalar paredes. A força do Homem-Aranha lhe permite erguer 10 toneladas ou mais. Graças a esta grande força Homem-Aranha ele pode realizar saltos inacreditáveis. Um sentido de aranha, que permite que ele saiba se o perigo está pairando sobre ele, antes que aconteça. Às vezes, isso pode levar o Homem-Aranha à fonte do perigo",
      img: "assets/img/spiderman.png",
      aparicao: "1962-08-01",
      casa: "Marvel"
    },
    {
      nome: "Wolverine",
      bio: "No universo ficcional da Marvel, Wolverine tem poderes regenerativos que podem curar qualquer ferida, não importa quão mortal seja, e esse mesmo poder o torna imune a qualquer doença existente na Terra e alguns extraterrestres. Ele também possui força sobre-humana Embora não se compare a outros super-heróis como o Hulk, supera o de qualquer humano. ",
      img: "assets/img/wolverine.png",
      aparicao: "1974-11-01",
      casa: "Marvel"
    }
  ];

  constructor() {
    this.heroes.forEach((hero: Hero, idx: number) => hero.idx = idx);
  }

  getHeroes(): Hero[] {
    return this.heroes;
  }

  getHero(idx: string): Hero {
    return this.heroes
      .find((hero: Hero) => hero.idx === parseInt(idx));
  }

  searchHeroes(term: string): Hero[] {
    term = term.toLowerCase();
    return this.heroes
      .filter((hero: Hero) => hero.nome.toLowerCase().indexOf(term) >= 0);
  }
}

export interface Hero {
  idx?: number;
  nome: string;
  bio: string;
  img: string;
  aparicao: string;
  casa: string;
};
