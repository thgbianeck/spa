import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { HeroesService, Hero } from '../../services/heroes.service';


@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
})
export class HeroComponent {

  hero: Hero;
  heroHouseUrl: string;

  constructor(
    private _heroesService: HeroesService,
    private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.hero = this._heroesService.getHero(params['id'])
      this.heroHouseUrl = this.heroHouse();
    })
  }

  heroHouse() {
    const casa = this.hero.casa.toLowerCase();
    return `assets/img/${casa}-logo.${casa === 'dc' ? 'jpg' : 'png'}`;
  }


}
