import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero, HeroesService } from 'src/app/services/heroes.service';



@Component({
  selector: 'app-search-engine',
  templateUrl: './search-engine.component.html',
})
export class SearchEngineComponent implements OnInit {

  heroes: Hero[] = [];
  term: string;

  constructor(
    private _heroesService: HeroesService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.term = params['term'];
      this.heroes = this._heroesService.searchHeroes(this.term);
    })
  }

}
